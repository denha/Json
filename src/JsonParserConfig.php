<?php

declare(strict_types=1);

namespace denha\Json\Json;

use ReflectionProperty;

class JsonParserConfig
{

    /**
     * CamelCase   : personId，序列化后属性：persionId
     * PascalCase  : personId，序列化后属性：PersonId
     * SnakeCase   : personId，序列化后属性：person_id
     * KebabCase   : personId，序列化后属性：person-id
     * @var string 序列化类型
     */
    protected $_propertyNamingStrategy = '';

    /** @var array 自定义过滤属性 */
    protected $_filterProperty = [];

    /** @var array 忽略属性元素 不处理 不返回 */
    protected $_ignoreFiles = [];

    /** @var bool 是否隐藏ull值元素 */
    protected $_ignoreNull = false;

    /** @var array 元素属性 */
    protected static $_reflectionPropertys = [];

    /**
     * @param JsonParserConfig
     * @return $this
     */
    public function setConfig(JsonParserConfig $config)
    {
        if ($config->_propertyNamingStrategy) {
            $this->_propertyNamingStrategy = $config->_propertyNamingStrategy;
        }

        if ($config->_filterProperty) {
            $this->_filterProperty = $config->_filterProperty;
        }

        return $this;
    }

    /**
     * CamelCase   : personId，序列化后属性：persionId
     * PascalCase  : personId，序列化后属性：PersonId
     * SnakeCase   : personId，序列化后属性：person_id
     * KebabCase   : personId，序列化后属性：person-id
     * @return $this
     */
    public function setPropertyNamingStrategy(string $value)
    {
        $this->_propertyNamingStrategy = $value;

        return $this;
    }

    /**
     * 设置忽略元素名单
     *
     * @param array $vals
     * @return $this
     */
    public function withIgnoreFiles(array $vals)
    {
        $this->_ignoreFiles = $vals;

        return $this;
    }


    /**
     * 设置过滤属性
     *
     * @param array 
     * @return $this
     */
    public function withFilterProperty(array $propes)
    {
        $this->_filterProperty = $propes;

        return $this;
    }

    /**
     * 忽略元素值为null的元素
     *
     * @param array 
     * @return $this
     */
    public function enableIgnoreNull()
    {
        $this->_ignoreNull = true;

        return $this;
    }

    /**
     * 获取最终属性名称
     *
     * @param ReflectionProperty $name
     * @return string|null
     */
    public function getPropertyName(ReflectionProperty $property): ?string
    {
        return $this->getPropertyNameByFilterProperty($property);
    }

    /**
     * 根据过滤条件获取属性名称 过滤内部定义属性
     *
     * @param ReflectionProperty $name
     * @return string|null
     */
    public function getPropertyNameByFilterProperty(ReflectionProperty $property): ?string
    {
        
        if($property->isProtected()){
            return null;
        }
        
        if(in_array($property->getName(),$this->_filterProperty)){
            return null;
        }
        
        $propertyName = $this->getPropertyNameByPropertyNamingStrategy($property->getName());

        return $propertyName;
    }

    /**
     * 转换属性名称
     *
     * @param string $name
     * @return string
     */
    public function getPropertyNameByPropertyNamingStrategy(string $name): string
    {

        switch ($this->_propertyNamingStrategy) {
            case 'CamelCase':
                $propertName = lcfirst(preg_replace_callback('/[_|-]([a-zA-Z])/', function ($match) {
                    return strtoupper($match[1]);
                }, $name));
                break;
            case 'PascalCase':
                $propertName = ucfirst(preg_replace_callback('/[_|-]([a-zA-Z])/', function ($match) {
                    return strtoupper($match[1]);
                }, $name));
                break;
            case 'SnakeCase':
                $propertName = strtolower(trim(preg_replace('/[A-Z]/', '_\\0', $name), '_'));
                break;
            case 'KebabCase':
                $propertName = strtolower(trim(preg_replace('/[A-Z]/', '-\\0', $name), '-'));
                break;
            default:
                $propertName = $name;
        }

        return $propertName;
    }

    /**
     * 获取反射元素类
     *
     * @param string $name
     * @return ReflectionProperty
     */
    protected function getReflectionProperty(string $name): ReflectionProperty
    {

        $key = get_class($this);
        if (!isset(self::$_reflectionPropertys[$key][$name])) {
            self::$_reflectionPropertys[$key][$name] = new ReflectionProperty($this, $name);
        }

        return self::$_reflectionPropertys[$key][$name];
    }
}
