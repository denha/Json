<?php

declare(strict_types=1);

namespace denha\Json\Json;


use denha\Json\Validated\ValidatedClient;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\ArrayCollection;

abstract class Json extends JsonParserConfig
{

    /** @var ArrayCollection */
    protected $_jsonArrayCollection;

    /** @var  bool 是否开启参数校验 */
    protected $_isOpenValidated = true;

    /** @var  bool 是否开启拷贝模式 拷贝模式禁用初始化set */
    protected $_isOpenPropertiesCopy = false;

    /**
     * 相同属性拷贝
     *
     * @param object $object
     * @return $this
     */
    public function copyProperties(object $object)
    {
        foreach ($object as $propertName => $value) {
            if (property_exists($this, $propertName) && !$this->getReflectionProperty($propertName)->isProtected()) {
                $this->{$propertName} = $value;
            }
        }

        return $this;
    }

    /**
     * 校验当前对象所有属性值是否为null
     *
     * @return boolean
     */
    public function checkObjectIsNull() : bool
    {
        foreach ($this as $propertName => $value) {
            if (!$this->getReflectionProperty($propertName)->isProtected() && $this->{$propertName} !== null ) {
                return false;
            }
        }

        return true;
    }

    /**
     * 相同属性拷贝
     *
     * @param Json $jsonObject
     * @return $this
     */
    public function copyPropertiesByJson(Json $jsonObject)
    {
        $this->_isOpenValidated = false;
        $this->_isOpenPropertiesCopy = true;

        $this->parseByArray($jsonObject->toArray());

        $this->_isOpenValidated = true;
        $this->_isOpenPropertiesCopy = false;

        return $this;
    }

    /**
     * json字符串转换对象值
     *
     * @param string $vals
     * @return $this
     */
    public function parseByJsonString(string $vals)
    {
        $vals = json_decode($vals, true);

        return $this->parseByArray($vals);
    }

    /**
     * 当前类执行参数校验 适用与直接创建实体后进行校验
     *
     * @return void
     */
    public function validatedClass()
    {
        foreach ($this as $propertName => $value) {

            $name =  $this->getPropertyName($this->getReflectionProperty($propertName));
            $docComment = $this->getReflectionProperty($propertName)->getDocComment();
            $ignore = $docComment ? preg_match('#@ignore#', $docComment) : false;

            if ($name && !$ignore) {
                $this->Validated($propertName, $value);
            }
        }
    }

    /**
     * 数组转换对象值
     *
     * @param array $vals
     * @return $this
     */
    public function parseByArray(array $vals)
    {
        foreach ($this as $propertName => $value) {

            $name =  $this->getPropertyName($this->getReflectionProperty($propertName));

            $docComment = $this->getReflectionProperty($propertName)->getDocComment();
            $ignore = $docComment ? preg_match('#@ignore#', $docComment) : false;

            // 过滤忽略的元素属性
            if ($name === null) {
                continue;
            }
            // 没有忽略的属性
            else if (!$ignore) {

                /** @var mixd 属性值 */
                $inputValue = $value;

                // 如果存在匹配的属性 更改初始属性值
                if (($checkPropertName = array_key_exists($propertName, $vals)) || array_key_exists($name, $vals)) {
                    $inputValue = $checkPropertName ? $vals[$propertName] :  $vals[$name];
                    $inputValue = is_array($inputValue) ? json_encode($inputValue, JSON_UNESCAPED_UNICODE) : $inputValue;
                }

                // if($propertName == 'auto_mode'){
                //     print_r($vals);
                //     var_dump(array_key_exists($propertName, $vals));
                //     var_dump($checkPropertName);
                //     var_dump($propertName);
                //     print_r($this);
                //     // print_r($vals);
                //     die;
                // }

                // 参数校验
                $this->Validated($propertName, $inputValue);

                // 参数赋值
                if ($inputValue !== $value) {
                    $this->setPropertAction($propertName, $inputValue);
                }
            }
            // 如果忽略了当前元素 会初始化一次setter方法 注意不会初始化getter
            // 拷贝模式不执行这个流程 如果执行这个流程结果不可预知
            else if ($ignore && !$this->_isOpenPropertiesCopy) {
                $this->setPropertAction($propertName, $value);
            }
        }

        return $this;
    }

    /**
     * 开启参数校验
     *
     * @return $this
     */
    public function enableValidated()
    {
        $this->_isOpenValidated = true;

        return $this;
    }

    /**
     * 禁用参数校验
     *
     * @return $this
     */
    public function disabledValidated()
    {
        $this->_isOpenValidated = false;

        return $this;
    }

    /**
     * 参数校验
     *
     * @return void
     * 
     * @throws denha\Json\Validated\Exception\ValidatedException
     */
    public function Validated(string $classPropertName, &$value)
    {
        if (!$this->_isOpenValidated) {
            return;
        }

        $ValidatedClient = new ValidatedClient();
        $isValidated = $ValidatedClient->checkValidated($this);

        if (!$isValidated) {
            $this->_isOpenValidated = false;
            return;
        }

        $handle =  $ValidatedClient->buildHandleByAnnotation($this->getReflectionProperty($classPropertName));
        if ($handle) {
            $handle->handleRequest($this->getReflectionProperty($classPropertName), $value);
        }
    }

    /**
     * Undocumented function
     *
     * @return ArrayCollection
     */
    public function getCollection(): ArrayCollection
    {
        return $this->_jsonArrayCollection ? $this->_jsonArrayCollection : new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @param mxid $params
     * @return array
     * @return ArrayCollection<string,mixd>
     * @deprecated 暂时废弃 没想好咋写
     */
    public function toCollection($params = null, &$maps = null): ?ArrayCollection
    {
        return null;
    }


    /**
     * 转换数组
     *
     * @param [type] $params
     * @return array
     */
    public function toArray($params = null, &$maps = null): array
    {

        $props = $params === null ? $this : $params;

        if ($maps === null) {
            $maps = [];
        }

        foreach ($props as $propertName => $value) {

            $name =  $this->getPropertyName($props->getReflectionProperty($propertName));

            if (
                $name === null
                || ($value === null && $this->_ignoreNull)
                || in_array($name, $this->_ignoreFiles)
            ) {
                continue;
            }

            $type = $this->getTypeByPropertValue($props->{$propertName});

            // 如果存在重写 则直接调用重写方法
            if (method_exists($props, 'get' . ucwords($propertName))) {
                $maps[$name] = $props->getPropertAction($propertName, $props);
            } else if ($type == 'string') {
                $maps[$name] = $props->getPropertAction($propertName, $props);
            } else if ($type == 'list') {
                foreach ($props->{$propertName} as $key => $item) {
                    $maps[$name][$key] = [];
                    $this->toArray($item, $maps[$name][$key]);
                }
            } else {
                $this->toArray($props->{$propertName}, $maps[$name]);
            }
        }

        return  $maps;
    }

    /**
     * 判断值是否继续调用当前方法
     *
     * @param mixd $val
     * @return boolean
     */
    private function getTypeByPropertValue($val): string
    {
        if (is_object($val)) {
            return 'object';
        } else if (is_array($val) && is_object(reset($val))) {
            return 'list';
        }

        return 'string';
    }

    public function  __call($method, $params)
    {
        $propertName = lcfirst(substr($method, 3));

        if (strncasecmp($method, 'get', 3) === 0) {
            return $this->$propertName;
        }

        if (strncasecmp($method, 'set', 3) === 0) {
            $this->$propertName = $params[0];
        }
    }

    /**
     * 调用_call方法 执行 get + 类属性名称方法
     *
     * @param string $classPropertName
     * @return void
     */
    private function getPropertAction(string $classPropertName, object $obj = null)
    {
        $action = 'get' . $classPropertName;

        if ($obj) {
            return $obj->$action();
        }

        return  $this->$action();
    }

    /**
     * 调用_call方法 set + 类属性名称方法
     *
     * @param string $classPropertName
     * @param mixd $value
     * @return void
     */
    private function setPropertAction(string $classPropertName, $value = null)
    {
        $action = 'set' . $classPropertName;

        return $this->$action($value);
    }
}
